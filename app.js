const express = require('express')
const runReqToBD = require('./src/runReqToBD').runReqToBD
const app = express()

const host = '127.0.0.1'
const port = 7100



app.get('/api/v1/questions', async (req, res)=> {
  let result = await runReqToBD('db_questions', 'questions')

  console.log(result)
  return res.json(result)
}) 

app.get('/api/v1/f_types', async (req, res)=> {
  let result = await runReqToBD('db_f_types', 'f_sp2')

  return res.json(result)
}) 

app.get('/api/v1/data_sp2', async (req, res)=> {
  let result = await runReqToBD('db_f_types', 'data_sp2')

  return res.json(result)
}) 


app.listen(port, host, () =>
console.log(`PLZ DB Server start http://${host}:${port}`)
)
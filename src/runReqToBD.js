const MongoClient = require('mongodb').MongoClient
const db_url = 'mongodb://localhost:27017'

async function runReqToBD(db = 'db_questions', collection = 'questions') {
    const client = new MongoClient(db_url)

    try {
      let data = []
      const dbClients = await client.db(db)
      const clientsCollection = await dbClients.collection(collection)
      data = await clientsCollection.find().toArray()
      await client.close();
      console.log(data)
      return data
    } catch(e) {
      console.log(e)
    }
  }

exports.runReqToBD = runReqToBD
